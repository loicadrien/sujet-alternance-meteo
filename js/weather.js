$(document).ready(function () {
    $("#result").hide();
    $("#hideposition").hide();
    $("#search").on("click", function (e) {
        var  searchValue = $("#ville").val();
        $("#error").html("");
        $("#tbody").empty();
                if(searchValue !== ''){

                    $.ajax({

                        url:'http://api.openweathermap.org/data/2.5/find?q='+searchValue+"&units=metric"+"&APPID=e81868d18283f45979475934b9b43df2",
                        type: "GET",
                        dataType: "jsonp",
                        success: function (data) {

                            //var temperature = data.list[0].name;
                            $("#result").fadeIn(1000);
                            for(var i=0; i<data.list.length; i++){
                                $("#tbody").append("<tr>\n" +" <th scope=\"row\">"+data.list[i].name +"</th>\n" +
                                    " <td>"+ data.list[i].sys.country +"</td>\n" +
                                    " <td>"+data.list[i].main.temp +" °C</td>\n" +
                                    " <td>"+data.list[i].weather[0].description +"</td>\n" +
                                    "<td><img src='http://openweathermap.org/img/w/"+data.list[i].weather[0].icon +".png'/></td>\n"+
                                    "<td><a href='https://www.openstreetmap.org/#map=14/'"+data.list[i].coord.lat +"'/'"+ data.list[i].coord.lon +">["+data.list[i].coord.lat +";"+data.list[i].coord.lon +"]</a> </td>\n" +
                                    " </tr>");
                            }
                            $("#ville").val("");
                            $("#error").empty();
                            console.log(data);

                        },
                            error(data) {
                                console.log(data);
                                $("#error").html("<div class='alert alert-danger center'> <strong>cette ville est inexistante ou mal orthographiée :)</strong></div>");
                                $("#result").fadeOut(200);
                            }


                    });
                }else{
                    $("#error").html("Aucune entrée");
                    $("#result").hide();
                }
    });


    $("#myposition").on("click", function(){

        var x = navigator.geolocation;
        x.getCurrentPosition(success, failure);
        var lat = null;
        var lon = null;
        function success(position){
            lat = position.coords.latitude;
            lon = position.coords.longitude;

            $.ajax({
                url: 'http://api.openweathermap.org/data/2.5/weather?lat='+ lat +'&lon='+lon+"&units=metric"+"&APPID=e81868d18283f45979475934b9b43df2",
                type: "GET",
                dataType: "jsonp",
                success: function (data) {
                    $("#logo").slideUp().delay(1000).slideDown().append("<p id=\"info\" class=\"text-center\"><strong>Ville: </strong>"+ data.name + " | <strong>Temp: </strong>"+ data.main.temp +"°<img src='http://openweathermap.org/img/w/"+data.weather[0].icon +".png'/>| <strong>Description:</strong>"+ data.weather[0].description +"</p>\n");
                    console.log(data);
                    $("#hideposition").show();
                    $("#myposition").hide();
                    $('#lat').empty();
                }
            });
        }
        function failure() {
            $('#lat').html("<p> coordonnées non valide</p>")
        }


    });
    $("#hideposition").on("click", function () {
        $("#myposition").show();
        $("#hideposition").hide();
        $("#logo").empty();
    })


});
